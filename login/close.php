<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Refresh" content="1;url=login.php">
</head>
<body>

<?php
// remove all session variables
session_unset(); 

// destroy the session 
session_destroy();

//echo "All session variables are now removed, and the session is destroyed." 
?>

</body>
</html>